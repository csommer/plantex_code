import os
import sys
import glob
import numpy
import pandas
import tifffile
np = numpy
pd = pandas

from matplotlib import pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

from scipy import optimize
from sklearn.cluster import OPTICS
from skimage import filters, measure
from skimage.feature import peak_local_max
from skimage.exposure import rescale_intensity
from sklearn.metrics import pairwise_distances



def detect_and_cluster(img_fn, sigma=0.7, threshold_rel=0.1, min_cluster_size=10, perc=99.99):
    """ Find vesicle centers and cluster them due to spatial proximity

    Arguments:
        img_fn {[str]} -- Image file name to

    Keyword Arguments:
        sigma {float}          -- [Sigma of Gaussian smoothing] (default: {0.7})
        threshold_rel {float}  -- [Relative threshold to filter found local maxima] (default: {0.1})
        min_cluster_size {int} -- [Minimum cluster size for OPTICS clustering] (default: {10})
        perc {float}           -- [Percentile for image normalization] (default: {99.99})
    """

    img_base_fn = os.path.splitext(os.path.basename(img_fn))[0]
    img_base_dir = os.path.dirname(img_fn)
    print("detect_and_cluster():", img_base_fn)

    img = tifffile.imread(img_fn)
    img = rescale_intensity(img, in_range=(img.min(), numpy.percentile(img.ravel(), perc)), out_range="uint8")
    imgf = filters.gaussian(img, sigma)

    peaks_ind = peak_local_max(imgf, min_distance=2, threshold_rel=threshold_rel, indices = True, exclude_border=False)

    print("  -- found total peaks:", peaks_ind.shape[0])

    dbs = OPTICS(min_cluster_size=min_cluster_size)
    dbs.fit(peaks_ind)
    labels = dbs.labels_ + 2

    print("  -- found clusters:", dbs.labels_.max() +1)
    print("  -- cluster sizes:", numpy.bincount(labels[labels>1])[2:])

    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax.scatter(*peaks_ind[labels >1, :].T, c=labels[labels >1], cmap="gist_ncar")

    df = pandas.DataFrame(peaks_ind, columns=["dim0", "dim1", "dim2"])
    df["labels"] = labels
    df["intensity"] = imgf[peaks_ind[:,0], peaks_ind[:,1], peaks_ind[:,2]]
    df.to_csv(os.path.join(img_base_dir, img_base_fn + "_clusters.tab"), sep="\t")
    
    
    img_out = numpy.stack((img, numpy.zeros_like(img)), axis=1)

    for pi, p in enumerate(peaks_ind):
#         img_out[p[0]-1:p[0]+1, 1,  p[1]-1:p[1]+1, p[2]-1:p[2]+1] = labels[labels > -1][pi] + 1
        img_out[p[0], 1,  p[1], p[2]] = labels[pi]
        
    
    tifffile.imsave(os.path.join(img_base_dir, img_base_fn + "_clusters.tif"), img_out, imagej=True, metadata={'Composite mode': 'composite'})

    return df


def crop_at(im, y, x, z, radius=3):
    dims = [int(y), int(x), int(z)]
    for i, d in enumerate(dims):
        assert (d-radius > -1) and d+radius < im.shape[i], "crop outside of array"

    return im[dims[0]-radius:dims[0]+radius+1,
              dims[1]-radius:dims[1]+radius+1,
              dims[2]-radius:dims[2]+radius+1]

def crop_at_2d(im, y, x, radius=3):
    dims = [int(y), int(x)]
    for i, d in enumerate(dims):
        assert (d-radius > -1) and d+radius < im.shape[i], "crop outside of array"

    return im[dims[0]-radius:dims[0]+radius+1,
              dims[1]-radius:dims[1]+radius+1]

def gaussian3d(height, yc, xc, zc,
                       yw, xw, zw):

    return lambda y,x,z: height * numpy.exp( - ( ((xc-x)/xw)**2
                                                +((yc-y)/yw)**2
                                                +((zc-z)/zw)**2 )
                                                /2)

def gaussian3d_iso(height, yc, xc, zc, w):

    return lambda y,x,z: height * numpy.exp( - ( ((xc-x)/w)**2
                                                +((yc-y)/w)**2
                                                +((zc-z)/w)**2 )
                                                /2)

def gaussian2d(height, yc, xc,
                       yw, xw):

    return lambda y,x: height * numpy.exp( - ( ((xc-x)/xw)**2
                                                +((yc-y)/yw)**2)
                                                /2)

def gaussian2d_iso(height, yc, xc, w):

    return lambda y,x: height * numpy.exp( - ( ((xc-x)/w)**2
                                                +((yc-y)/w)**2)
                                                /2)

def fit_gaussian3d(data):
    params = (data.max(),) + tuple(numpy.array(data.shape) / 2) + (1,)*len(data.shape)
    errorfunction = lambda p: numpy.ravel(gaussian3d(*p)(*numpy.indices(data.shape)) - data)
    p, success = optimize.leastsq(errorfunction, params, maxfev = 20000)
    return p, success

def fit_gaussian3d_iso(data):
    params = (data.max(),) + tuple(numpy.array(data.shape) / 2) + (1,)
    errorfunction = lambda p: numpy.ravel(gaussian3d_iso(*p)(*numpy.indices(data.shape)) - data)
    p, success = optimize.leastsq(errorfunction, params, maxfev = 20000)
    return p

def fit_gaussian2d(data):
    params = (data.max(),) + tuple(numpy.array(data.shape) / 2) + (1,)*len(data.shape)
    errorfunction = lambda p: numpy.ravel(gaussian2d(*p)(*numpy.indices(data.shape)) - data)
    p, success = optimize.leastsq(errorfunction, params, maxfev = 20000)
    return p

def fit_gaussian2d_iso(data):
    params = (data.max(),) + tuple(numpy.array(data.shape) / 2) + (1,)
    errorfunction = lambda p: numpy.ravel(gaussian2d_iso(*p)(*numpy.indices(data.shape)) - data)
    p, success = optimize.leastsq(errorfunction, params, maxfev = 20000)
    return p

def isolated_points(ctab, min_dist=7, coords=None):
    if not coords:
        coords = ["dim0", "dim1", "dim2"]
    pw = pairwise_distances(ctab[coords].values)
    pw[numpy.eye(pw.shape[0]) > 0] = pw.max()

    isolated_points_idx = (pw > min_dist).all(1)

    return ctab[isolated_points_idx].copy()
