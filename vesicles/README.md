# PlantEx code
---

### Vesicle quantification

The folder contains a IPython notebook and example images. Follow the steps in [PlantEx_vesicles.ipynb](./PlantEx_vesicles.ipynb).

The actual vesicle detection, fitting, and clustering code can be found in the [plantEx_code](./plantEx_code) Python package.