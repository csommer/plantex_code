# PlantEx code
---

### Expansion factor

The folder contains a Fiji (.groovy) script and example (BigWarp) 2D landmark files.

Run the script "[bigwarp_landmark2expansionfactor.groovy](bigwarp_landmark2expansionfactor.groovy)" in Fiji and select one of the example landmark files.