#@ File(label="Input image", style="file") img_fn
#@ File(label="Input cluster", style="file") clusters_fn
#@ File(label="Input density", style="file") density_fn
#@ SciView sciView
#@ OpService opService
#@ IOService ioService
#@ UIService uiService
#@ LUTService lutService

import net.imagej.*;
import ij.*;
import bigwarp.*;
import graphics.scenery.Material;
import graphics.scenery.Node;
import graphics.scenery.Sphere;
import graphics.scenery.Line;
import graphics.scenery.PointLight;
import net.imglib2.*;
import net.imglib2.util.*;
import net.imglib2.type.numeric.*;
import net.imglib2.realtransform.*;
import net.imglib2.display.AbstractArrayColorTable;
import com.jogamp.opengl.math.Quaternion;
import graphics.scenery.BoundingGrid;
import sc.iview.vector.*;
import cleargl.GLVector;
import net.imglib2.img.display.imagej.ImageJFunctions;
import graphics.scenery.backends.RenderedImage;
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.io.File;
import java.io.IOException;
import org.scijava.util.Colors;
import sc.iview.SciView;
import sc.iview.vector.ClearGLVector3;
import sc.iview.vector.Vector3;
import static sc.iview.commands.MenuWeights.VIEW;
import static sc.iview.commands.MenuWeights.VIEW_SCREENSHOT;
import org.scijava.io.IOService;

def RGBColors = [
                    [255,  0,  0],
                    [  0,255,  0],
                    [  0,  0,255],
                    
                    [255,255,  0],
                    [  0,255,255],
                    [255,  0,255],
                    
                    [255,127,  0],
                    [255,  0,127],
                    [  0,255,127],
                    
                    [127,255,  0],
                    [127,  0,255],
                    [0  ,127,255],
    
                    [255,255,127],
                    [127,255,255],
                    [255,127,255],
                ];

def add_vesicle_volume(img_fn, sciView, opService, lutService) { 
    vesicle_volume_imp = IJ.openImage(img_fn.absolutePath)
    vesicle_volume_im = ImageJFunctions.wrap(vesicle_volume_imp)
    vesicle_volume_im = opService.convert().uint8(vesicle_volume_im)
    vesicle_volume = sciView.addVolume( vesicle_volume_im );
    
    colorTable = lutService.loadLUT( lutService.findLUTs().get( "Red Hot.lut" ) );
    
    sciView.setColormap(vesicle_volume, (AbstractArrayColorTable) colorTable)
    
    vesicle_volume.setPixelToWorldRatio(0.1f);
    vesicle_volume.setDirty(true);
    vesicle_volume.setNeedsUpdate(true);
    
    rampMin = 0.0f
    rampMax = 0.016f
    
    tf = vesicle_volume.getTransferFunction();
    tf.clear();
    tf.addControlPoint(0.0f, 0.0f);
    tf.addControlPoint(rampMin, 0.0f);
    tf.addControlPoint(1.0f, rampMax);

    vesicle_volume.setName("Vesicle volume")

    return vesicle_volume;
    
    }

def add_density_volume(density_fn, sciView, opService, lutService) {
    denImgPlus = IJ.openImage(density_fn.absolutePath)
    denImg = ImageJFunctions.wrap(denImgPlus)
    denVolume = sciView.addVolume( denImg );
    
    colorTable = lutService.loadLUT( lutService.findLUTs().get( "Cyan.lut" ) );    
    sciView.setColormap(denVolume, (AbstractArrayColorTable) colorTable)
    
    denVolume.setPixelToWorldRatio(0.1f);
    denVolume.setDirty(true);
    denVolume.setNeedsUpdate(true);
    
    rampMin = 0.1f;
    rampMax = 0.005f;
    
    tf = denVolume.getTransferFunction();
    tf.clear();
    tf.addControlPoint(0.0f, 0.0f);
    tf.addControlPoint(rampMin, 0.0f);
    tf.addControlPoint(1.0f, rampMax);
    denVolume.setVisible(false)
    denVolume.setName("Density")
    return denVolume;
    }

def set_camera_15um_volume_default(sciView) {
    sciView.moveCamera(-13.2f, 
                        11.4f, 
                       -22.8f)
    sciView.getCamera().setRotation( new Quaternion().setFromEuler( -0.40f,
                                                                     2.6f,
                                                                     0.23f ) );
    }


    

def add_15um_volume_box(sciView) {
    double edgeWidth = 0.1;
    points = new Vector3[3];

    bbox = new Node();
    
    points[0] = new ClearGLVector3( ( float ) ( -7.5f ), ( float ) ( -7.5f ), ( float ) ( -7.5f ) );
    points[1] = new ClearGLVector3( ( float ) ( 0f ), ( float ) ( 0f ), ( float ) ( 15f ) );
    points[2] = new ClearGLVector3( ( float ) ( 0f ), ( float ) ( 0f ), ( float ) ( 0f ) );
    l = sciView.addLine( points, Colors.LIGHTSALMON, edgeWidth );
    l.getParent().removeChild(l)
    bbox.addChild(l);
    

    points[0] = new ClearGLVector3( ( float ) ( -7.5f ), ( float ) ( -7.5f ), ( float ) ( -7.5f ) );
    points[1] = new ClearGLVector3( ( float ) ( 0f ), ( float ) ( 15f ), ( float ) ( 0f ) );
    points[2] = new ClearGLVector3( ( float ) ( 0f ), ( float ) ( 0f ), ( float ) ( 0f ) );
    l = sciView.addLine( points, Colors.LIGHTSALMON, edgeWidth );
    l.getParent().removeChild(l)
    bbox.addChild(l);

    points[0] = new ClearGLVector3( ( float ) ( -7.5f ), ( float ) ( -7.5f ), ( float ) ( -7.5f ) );
    points[1] = new ClearGLVector3( ( float ) ( 15f ), ( float ) ( 0f ), ( float ) ( 0f ) );
    points[2] = new ClearGLVector3( ( float ) ( 0f ), ( float ) ( 0f ), ( float ) ( 0f ) );
    l = sciView.addLine( points, Colors.LIGHTSALMON, edgeWidth );
    l.getParent().removeChild(l)
    bbox.addChild(l);

    points[0] = new ClearGLVector3( ( float ) ( 7.5f ), ( float ) ( 7.5f ), ( float ) ( 7.5f ) );
    points[1] = new ClearGLVector3( ( float ) ( 0f ), ( float ) ( 0f ), ( float ) ( -15f ) );
    points[2] = new ClearGLVector3( ( float ) ( 0f ), ( float ) ( 0f ), ( float ) ( 0f ) );
    l = sciView.addLine( points, Colors.LIGHTSALMON, edgeWidth );
    l.getParent().removeChild(l)
    bbox.addChild(l);

    points[0] = new ClearGLVector3( ( float ) ( 7.5f ), ( float ) ( 7.5f ), ( float ) ( 7.5f ) );
    points[1] = new ClearGLVector3( ( float ) ( 0f ), ( float ) ( -15f ), ( float ) ( 0f ) );
    points[2] = new ClearGLVector3( ( float ) ( 0f ), ( float ) ( 0f ), ( float ) ( 0f ) );
    l = sciView.addLine( points, Colors.LIGHTSALMON, edgeWidth );
    l.getParent().removeChild(l)
    bbox.addChild(l);

    points[0] = new ClearGLVector3( ( float ) ( 7.5f ), ( float ) ( 7.5f ), ( float ) ( 7.5f ) );
    points[1] = new ClearGLVector3( ( float ) ( -15f ), ( float ) ( 0f ), ( float ) ( 0f ) );
    points[2] = new ClearGLVector3( ( float ) ( 0f ), ( float ) ( 0f ), ( float ) ( 0f ) );
    l = sciView.addLine( points, Colors.LIGHTSALMON, edgeWidth );
    l.getParent().removeChild(l)
    bbox.addChild(l);

    points[0] = new ClearGLVector3( ( float ) ( 7.5f ), ( float ) ( -7.5f ), ( float ) ( -7.5f ) );
    points[1] = new ClearGLVector3( ( float ) ( 0f ), ( float ) ( 15f ), ( float ) ( 0) );
    points[2] = new ClearGLVector3( ( float ) ( 0f ), ( float ) ( 0f ), ( float ) ( 0f ) );
    l = sciView.addLine( points, Colors.LIGHTSALMON, edgeWidth );
    l.getParent().removeChild(l)
    bbox.addChild(l);

    points[0] = new ClearGLVector3( ( float ) ( 7.5f ), ( float ) ( -7.5f ), ( float ) ( -7.5f ) );
    points[1] = new ClearGLVector3( ( float ) ( 0f ), ( float ) ( 0f ), ( float ) ( 15) );
    points[2] = new ClearGLVector3( ( float ) ( 0f ), ( float ) ( 0f ), ( float ) ( 0f ) );
    l = sciView.addLine( points, Colors.LIGHTSALMON, edgeWidth );

    l.getParent().removeChild(l)
    bbox.addChild(l);

    points[0] = new ClearGLVector3( ( float ) ( -7.5f ), ( float ) ( 7.5f ), ( float ) ( -7.5f ) );
    points[1] = new ClearGLVector3( ( float ) ( 0f ), ( float ) ( 0f ), ( float ) ( 15f) );
    points[2] = new ClearGLVector3( ( float ) ( 0f ), ( float ) ( 0f ), ( float ) ( 0f ) );
    l = sciView.addLine( points, Colors.LIGHTSALMON, edgeWidth );
    l.getParent().removeChild(l)
    bbox.addChild(l);

    points[0] = new ClearGLVector3( ( float ) ( -7.5f ), ( float ) ( 7.5f ), ( float ) ( -7.5f ) );
    points[1] = new ClearGLVector3( ( float ) ( 15f ), ( float ) ( 0f ), ( float ) ( 0f) );
    points[2] = new ClearGLVector3( ( float ) ( 0f ), ( float ) ( 0f ), ( float ) ( 0f ) );
    l = sciView.addLine( points, Colors.LIGHTSALMON, edgeWidth );
    l.getParent().removeChild(l)
    bbox.addChild(l);

    points[0] = new ClearGLVector3( ( float ) ( -7.5f ), ( float ) ( 7.5f ), ( float ) ( 7.5f ) );
    points[1] = new ClearGLVector3( ( float ) ( 0f ), ( float ) ( -15f ), ( float ) ( 0f) );
    points[2] = new ClearGLVector3( ( float ) ( 0f ), ( float ) ( 0f ), ( float ) ( 0f ) );
    l = sciView.addLine( points, Colors.LIGHTSALMON, edgeWidth );
    l.getParent().removeChild(l)
    bbox.addChild(l);
    

    points[0] = new ClearGLVector3( ( float ) ( -7.5f ), ( float ) ( -7.5f ), ( float ) ( 7.5f ) );
    points[1] = new ClearGLVector3( ( float ) ( 15f ), ( float ) ( 0f ), ( float ) ( 0f) );
    points[2] = new ClearGLVector3( ( float ) ( 0f ), ( float ) ( 0f ), ( float ) ( 0f ) );
    l = sciView.addLine( points, Colors.LIGHTSALMON, edgeWidth );
    l.getParent().removeChild(l)
    bbox.addChild(l);

    bbox.setName("Box");
    sciView.addNode(bbox);

    return bbox;

    }


def read_vesicle_tab(clusters_fn) {
        // read cluster
    def coords = []
    
    File csvFile = new File(clusters_fn.absolutePath)
    
    csvFile.eachLine { line, c ->
        if (c > 1) {
            def parts = line.split("\t")
            def tmpMap = [:]
        
            tmpMap.putAt("d0", Float.parseFloat(parts[1]))
            tmpMap.putAt("d1", Float.parseFloat(parts[2]))
            tmpMap.putAt("d2", Float.parseFloat(parts[3]))
            tmpMap.putAt("l",  Integer.parseInt(parts[4]))
            tmpMap.putAt("i",  Float.parseFloat(parts[5]))
            // etc.
        
            coords.add(tmpMap)
            }
        }
    return coords;
    }

def add_vesicle_spheres(coords, sciView) {
    def RGBColors = [
                    [255,  0,  0],
                    [  0,255,  0],
                    [  0,  0,255],
                    
                    [255,255,  0],
                    [  0,255,255],
                    [255,  0,255],
                    
                    [255,127,  0],
                    [255,  0,127],
                    [  0,255,127],
                    
                    [127,255,  0],
                    [127,  0,255],
                    [0  ,127,255],
    
                    [255,255,127],
                    [127,255,255],
                    [255,127,255],
                ];
    

    calibrationScale = 1.0;
    ox = 75;
    oy = 75;
    oz = 75;
    
    scale = 0.1f;
    
    Map vesicle = [:].withDefault { [] }

    vesicle_iso = new Node();
    vesicle_iso.setName("Non-clustered vesicle");

    vesicle_clust = new Node();
    vesicle_clust.setName("Clustered vesicle");

    
    coords.eachWithIndex { 
        val, idx -> 
        if (val["i"] > 0.0) {
            material = new Material();
            material.setAmbient( new GLVector( 1.0f, 0.0f, 0.0f ) );
            material.setDiffuse( new GLVector( 0.0f, 1.0f, 0.0f ) );
            material.setSpecular(new GLVector( 1.0f, 1.0f, 1.0f ) );
    
            Sphere sphere = new Sphere( 1, 20 );
            sphere.setMaterial( material );
            sphere.setPosition( ClearGLVector3.convert( new ClearGLVector3( (float)((  val["d2"]/calibrationScale - ox) * scale), 
                                                                            (float)(( val["d1"]/calibrationScale - oy) * scale), 
                                                                            (float)(( val["d0"]/calibrationScale - oz) * scale) ) ) );
            
            sphere.setVisible(true)
            sphere.setScale(new GLVector(scale,
                            scale,
                            scale))
    
           vesicle[val["l"]].add(sphere)


           if (val["l"] > 1) {
                vesicle_clust.addChild(sphere)
                }
           else {
                vesicle_iso.addChild(sphere)
                }

           
           }
       }

    vesicle[1].each { it.getMaterial().setDiffuse (new GLVector(0.5f,0.5f,0.5f));
                      it.getMaterial().setAmbient (new GLVector(1.0f,0.0f,0.0f));
                      it.getMaterial().setSpecular(new GLVector(1f,1f,1f));
                        }
    vesicle.each { key, val -> 
                        if (key > 1) {
                        rgb_col = RGBColors[(key) % RGBColors.size()]
                        val.each({
                             col_vec = new GLVector((float) (rgb_col[0] / 255f), (float) (rgb_col[1] / 255f), (float) (rgb_col[2] / 255f));
                             it.getMaterial().setDiffuse(col_vec) 
                             it.getMaterial().setSpecular(col_vec)
//                             it.getMaterial().setAmbient(col_vec)
                        });
                    }
                 }

    sciView.addNode(vesicle_clust);
    sciView.addNode(vesicle_iso);

    return [vesicle_clust, vesicle_iso]
    }

class Screenshooter {
    String path
    String name
    int counter
    int sleeping
    def sciView
    
    Screenshooter(path, name, sciView) {
        this.path = path
        this.name = name
        this.sciView = sciView
        
        this.counter = 1
        this.sleeping = 500
        }

    def shot() {
        sleep(this.sleeping)
        RenderedImage screenshot = this.sciView.getSceneryRenderer().requestScreenshot();
    
        BufferedImage image = new BufferedImage(screenshot.getWidth(), screenshot.getHeight(), BufferedImage.TYPE_4BYTE_ABGR);
        byte[] imgData = ((DataBufferByte) image.getRaster().getDataBuffer()).getData();
        System.arraycopy(screenshot.getData(), 0, imgData, 0, screenshot.getData().length);
    
        File tmpFile = null;
        tmpFile = new File(this.path + "/" + this.name + "_${this.counter}.png");
        println tmpFile.absolutePath
            ImageIO.write(image, "png", tmpFile);   

        this.counter += 1
        
        }
    }



def init_scene(img_fn, clusters_fn, density_fn, sciView, opService, lutService) {
    // disable floor
    sciView.getFloor().setVisible(false)

    // turn of last light
    lights = sciView.getSceneNodes({n -> n instanceof PointLight});
    lights[3].setVisible(false)

    // add vesicle volume
    vesicle_volume = add_vesicle_volume(img_fn, sciView, opService, lutService)
    
    // add density volume
    density_volume = add_density_volume(density_fn, sciView, opService, lutService) 

    // set cam postition
    set_camera_15um_volume_default(sciView)

    // add bounding box
    bounding_box = add_15um_volume_box(sciView)
    bounding_box.setVisible(true);

    // read vesicles from .tab file
    coords = read_vesicle_tab(clusters_fn)

    // add vesicles to scene
    (vesicle_cluster, vesicle_iso) = add_vesicle_spheres(coords, sciView)

    vesicle_cluster.setVisible(false) 
    vesicle_iso.setVisible(false) 

    return [bounding_box, vesicle_volume, density_volume, vesicle_cluster, vesicle_iso]
   
}


sciView.getRenderer().toggleFullscreen() // not working!?

(bounding_box, vesicle_volume, density_volume, vesicle_cluster, vesicle_iso) = init_scene(img_fn, clusters_fn, density_fn, sciView, opService, lutService)

/*
Screen = new Screenshooter("C:/Users/csommer/Desktop", "test", sciView)
//
Screen.shot()
//
vesicle_iso.setVisible(true)  
vesicle_cluster.setVisible(true)  
Screen.shot()
//
vesicle_iso.setVisible(false)  
Screen.shot()
//
vesicle_volume.setVisible(false)  
Screen.shot()
//
vesicle_volume.setVisible(true)  
Screen.shot()
*/






