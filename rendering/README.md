# PlantEx code
---

### Rendering of clustered vesicles

The folder contains a Fiji (.groovy) script

```
show_volume_vesicles.groovy
```

1. Make sure the `sciview' update-site is activated in your Fiji distribution
2. Execute .groovy script and supply path to
   * raw volume .tif file
   * extracted vesicles .csv file (generated in [Vesicle quantification](../vesicles))
3. Vesicles will be rendered (example below)
   * see SciView online help for controls (e. g. rotation: SHIFT+mouse-move)
   * visibility of clustered and non-clustered vesicles can be toggled from the menu

<img src="example.png" width="512">
