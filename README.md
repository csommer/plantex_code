# PlantEx code
---
This repository contains the code for the manuscript "Super-resolution expansion microscopy in plants" (unpublished)

by Michelle Gallei, Sven Truckenbrodt, Christoph Sommer, Eva Benková, Jiří Friml, Johann G. Danzl

Institute of Science and Technology Austria, Am Campus 1, 3400 Klosterneuburg, Austria

### Items

* [Expansion factor](./expansion_factor)

    Extracting the expansion factor from BigWarp aligned pre- and PlantEx images (2D/3D) using a linear Similarity transform.

    The folder contains a Fiji (.groovy) script and example (2D) landmark files.


* [Distortion analysis](./distortion)

    Estimation and quantification of local distortions after expansion by optical flow algorithm.

    The folder contains a IPython notebook and example images.


* [Vesicle quantification](./vesicles)

    Detection and quantification of vesicles in PlantEx images.

    The folder contains a IPython notebook and example images.

* [Vesicle rendering](./rendering)

    Visualizaton of clustered vesicles with the original volume

    The folder contains a Fiji script (.groovy) that takes the raw volume and the clustered vesicles as csv table

    (Note, Fiji/Sciview required, enable Sciview in your update-sites)


### Requirements

1. Fiji/ImageJ with BigWarp plug-in (update-site)
2. Python >=3.6 (Anaconda distribution) with installed libraries:
   * skimage (image processing)
   * sklearn (clusterimg)
   * scipy (curve fitting)
   * pandas (DataFrames)
   * matplotlib (plotting)
   * seaborn (plotting)
   * tifffile (image IO)
   * opencv (optical flow)

### Installation

```
# clone repository
git clone https://git.ist.ac.at/csommer/plantex_code.git

# code available in subdirectories
cd plantex_code
```






